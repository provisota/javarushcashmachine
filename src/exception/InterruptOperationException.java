package exception;

/**
 * @author Alterovych Ilya
 */
public class InterruptOperationException extends Exception {
    private static final long serialVersionUID = -7816803114783465275L;
}
