package exception;

/**
 * @author Alterovych Ilya
 */
public class NotEnoughMoneyException extends Exception {
    private static final long serialVersionUID = 6710165794721940629L;
}
