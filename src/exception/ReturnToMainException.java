package exception;

/**
 * @author Alterovych Ilya
 */
public class ReturnToMainException extends Exception {
    private static final long serialVersionUID = -1191097902513266052L;
}
