package main;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Alterovych Ilya
 */
public class CurrencyManipulatorFactory {

    private CurrencyManipulatorFactory() {
    }

    private static Map <String, CurrencyManipulator> manipulatorsMap = new HashMap<>();

    public static CurrencyManipulator
    getManipulatorByCurrencyCode(String currencyCode){

        if (manipulatorsMap.get(currencyCode) == null){
           manipulatorsMap.put(currencyCode, new CurrencyManipulator(currencyCode));
        }
        return manipulatorsMap.get(currencyCode);
    }

    public static boolean isCurrencyPresent(String currencyCode){
        return manipulatorsMap.get(currencyCode) != null;
    }

    public static Collection<CurrencyManipulator> getAllCurrencyManipulators(){
        return manipulatorsMap.values();
    }
}
