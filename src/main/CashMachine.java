package main;

import command.CommandExecutor;
import exception.InterruptOperationException;
import exception.ReturnToMainException;

import java.util.Locale;

/**
 * @author Alterovych Ilya
 */
public class CashMachine {
    public static final String RESOURCE_PATH = "resources.";

    public static void main(String[] args) {
        Locale.setDefault(Locale.ENGLISH);
        try {
            Operation operation;
//            CommandExecutor.execute(Operation.LOGIN);
            do {
                try {
                    operation = ConsoleHelper.askOperation();
                    CommandExecutor.execute(operation);
                } catch (ReturnToMainException ignored) {
                }
            }
            while (true);
        } catch (InterruptOperationException e) {
            ConsoleHelper.printExitMessage();
        }
    }
}
