package main;

import exception.NotEnoughMoneyException;

import java.util.*;

/**
 * @author Alterovych Ilya
 */
public class CurrencyManipulator {
    private String currencyCode;
    private Map<Integer, Integer> denominations = new HashMap<>();

    public CurrencyManipulator(String currencyCode) {
        this.currencyCode = currencyCode;
//        denominations.put(1, 1000);
//        denominations.put(500, 2);
//        denominations.put(200, 1);
//        denominations.put(100, 1);
//        denominations.put(50, 2);
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void addAmount(int denomination, int count) {
        if (denominations.containsKey(denomination)) {
            denominations.put(denomination, denominations.get(denomination) + count);
        } else {
            denominations.put(denomination, count);
        }
    }

    public int getTotalAmount() {
        int totalAmount = 0;

        for (Integer denomination : denominations.keySet()) {
            totalAmount += denomination * denominations.get(denomination);
        }

        return totalAmount;
    }

    public boolean isAmountAvailable(int expectedAmount) {
        return getTotalAmount() >= expectedAmount;
    }

    public boolean hasMoney() {
        return (!denominations.isEmpty());
    }

    public Map<Integer, Integer> withdrawAmount(int expectedAmount)
            throws NotEnoughMoneyException {

        int moneyCounter = expectedAmount;
        Map<Integer, Integer> withdrawDenominations = new HashMap<>();
        Map<Integer, Integer> sortedDenominations =
                new TreeMap<>(new Comparator<Integer>() {
                    @Override
                    public int compare(Integer o1, Integer o2) {
                        return o2.compareTo(o1);
                    }
                });
        sortedDenominations.putAll(denominations);

        for (Integer denomination : sortedDenominations.keySet()) {
            while (denomination <= moneyCounter && denomination != 0
                    && sortedDenominations.get(denomination) > 0) {
                if (withdrawDenominations.containsKey(denomination)) {
                    moneyCounter -= denomination;
                    withdrawDenominations.put(denomination,
                            withdrawDenominations.get(denomination) + 1);
                    sortedDenominations.put(denomination,
                            sortedDenominations.get(denomination) - 1);
                } else {
                    moneyCounter -= denomination;
                    withdrawDenominations.put(denomination, 1);
                    sortedDenominations.put(denomination,
                            sortedDenominations.get(denomination) - 1);
                }
            }
            if (moneyCounter == 0) {
                break;
            }
        }

        if (moneyCounter > 0) {
            throw new NotEnoughMoneyException();
        }

        for (Integer denomination : withdrawDenominations.keySet()) {
            denominations.put(denomination, denominations.get(denomination)
                    - withdrawDenominations.get(denomination));
        }

        return withdrawDenominations;
    }
}
