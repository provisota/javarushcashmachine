package main;

import exception.InterruptOperationException;
import exception.ReturnToMainException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ResourceBundle;

/**
 * @author Alterovych Ilya
 */
public class ConsoleHelper {
    private static ResourceBundle res =
            ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "common");

    private static BufferedReader reader =
            new BufferedReader(new InputStreamReader(System.in));

    public static void reloadResources(){
        res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "common");
    }

    public static void writeMessage(String message) {
        System.out.println(message);
    }

    public static void printExitMessage(){
        writeMessage(res.getString("the.end"));
    }

    public static String readString() throws
            InterruptOperationException, ReturnToMainException {
        String line = null;
        try {
            line = reader.readLine();
            if ("exit".equalsIgnoreCase(line)){
                throw new InterruptOperationException();
            }
            if ("main".equalsIgnoreCase(line)){
                throw new ReturnToMainException();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return line;
    }

    public static String askCurrencyCode() throws
            InterruptOperationException, ReturnToMainException {
        String currencyCode;
        while (true) {
            writeMessage(res.getString("choose.currency.code"));
            currencyCode = readString();
            if (currencyCode.length() != 3) {
                writeMessage(res.getString("invalid.data"));
            } else {
                currencyCode = currencyCode.toUpperCase();
                break;
            }
        }
        return currencyCode;
    }

    public static String[] getValidTwoDigits(String currencyCode)
            throws InterruptOperationException, ReturnToMainException {
        String[] validTwoDigits = new String[2];
        String line;
        boolean correct = false;

        while (!correct) {
            writeMessage(String.format
                    (res.getString("choose.denomination.and.count.format")
                            , currencyCode));
            line = readString();
            validTwoDigits = line.split(" ");
            if (validTwoDigits.length != 2) {
                writeMessage(res.getString("invalid.data"));
                continue;
            }
            for (String validTwoDigit : validTwoDigits) {
                try {
                    int number = Integer.parseInt(validTwoDigit);
                    if (number < 0)
                        throw new NumberFormatException();
                } catch (NumberFormatException e) {
                    correct = false;
                    writeMessage(res.getString("invalid.data"));
                    break;
                }
                correct = true;
            }
        }
        return validTwoDigits;
    }

    public static Operation askOperation() throws
            InterruptOperationException, ReturnToMainException {
        String line;
        int ordinal;
        Operation operation;

        while (true) {
            writeMessage(res.getString("choose.operation"));
            writeMessage(String.format("1 - %s, 2 - %s, 3 - %s, 4 - %s, 5 - %s"
                    , res.getString("operation.INFO"), res.getString("operation.DEPOSIT")
                    , res.getString("operation.WITHDRAW")
                    , res.getString("operation.LANGUAGE")
                    , res.getString("operation.EXIT")));
            line = readString();
            try {
                ordinal = Integer.parseInt(line);
            } catch (NumberFormatException e) {
                writeMessage(res.getString("invalid.data"));
                continue;
            }
            try {
                operation = Operation.getAllowableOperationByOrdinal(ordinal);
            } catch (IllegalArgumentException e) {
                writeMessage(res.getString("invalid.data"));
                continue;
            }
            return operation;
        }
    }
}
