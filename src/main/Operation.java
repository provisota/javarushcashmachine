package main;

/**
 * @author Alterovych Ilya
 */
public enum Operation{
    LOGIN, INFO, DEPOSIT,
    WITHDRAW, LANGUAGE, EXIT;

    public static Operation getAllowableOperationByOrdinal(Integer i){
        if (i == 0){
            throw new IllegalArgumentException();
        }
        Operation[] values = Operation.values();
        for (Operation value : values){
            if ((value.ordinal()) == i){
                return value;
            }
        }
        throw new IllegalArgumentException();
    }
}
