package command;

import exception.InterruptOperationException;
import exception.ReturnToMainException;
import main.CashMachine;
import main.ConsoleHelper;
import main.CurrencyManipulatorFactory;

import java.util.ResourceBundle;

/**
 * @author Alterovych Ilya
 */
class DepositCommand implements Command{
    private ResourceBundle res =
            ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "deposit");
    @Override
    public void execute() throws InterruptOperationException, ReturnToMainException {
        ConsoleHelper.writeMessage(res.getString("before"));
        String currencyCode = ConsoleHelper.askCurrencyCode();

        String[] validTwoDigits = ConsoleHelper.getValidTwoDigits(currencyCode);
        int denomination = Integer.parseInt(validTwoDigits[0]);
        int count = Integer.parseInt(validTwoDigits[1]);
        ConsoleHelper.writeMessage(String.format(res.getString("success.format"),
                denomination, count));

        CurrencyManipulatorFactory.getManipulatorByCurrencyCode(currencyCode)
                .addAmount(denomination, count);

    }

    @Override
    public void reloadResources() {
        res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "deposit");
    }
}
