package command;

import exception.InterruptOperationException;
import exception.ReturnToMainException;
import main.Operation;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Alterovych Ilya
 */
public class CommandExecutor {
    private static Map<Operation, Command> commandMap = new HashMap<>();

    private CommandExecutor(){
    }

    static {
        commandMap.put(Operation.LOGIN, new LoginCommand());
        commandMap.put(Operation.INFO, new InfoCommand());
        commandMap.put(Operation.DEPOSIT, new DepositCommand());
        commandMap.put(Operation.WITHDRAW, new WithdrawCommand());
        commandMap.put(Operation.LANGUAGE, new LanguageCommand());
        commandMap.put(Operation.EXIT, new ExitCommand());
    }

    public static final void execute(Operation operation)
            throws InterruptOperationException, ReturnToMainException {
        commandMap.get(operation).execute();
    }

    public static final void reloadAllResources(){
        for (Operation operation : commandMap.keySet()) {
            commandMap.get(operation).reloadResources();
        }
    }
}
