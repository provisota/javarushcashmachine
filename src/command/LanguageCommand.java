package command;

import exception.InterruptOperationException;
import exception.ReturnToMainException;
import main.CashMachine;
import main.ConsoleHelper;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author Alterovych Ilya
 */
public class LanguageCommand implements Command {

    private static ResourceBundle res =
            ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "language");

    @Override
    public void execute() throws InterruptOperationException, ReturnToMainException {
        Locale ruLocale = new Locale("ru");
        int language = 0;

        while (true) {
            ConsoleHelper.writeMessage(res.getString("available.languages"));
            String input = ConsoleHelper.readString();
            if (!input.matches("^[0-9]+$")) {
                ConsoleHelper.writeMessage(res.getString("wrong.input.digit"));
                continue;
            }
            try {
                language = Integer.parseInt(input);
            } catch (NumberFormatException e) {
                ConsoleHelper.writeMessage(res.getString("wrong.input"));
                continue;
            }
            if (language == 1) {
                Locale.setDefault(ruLocale);
                break;
            }
            if (language == 2) {
                Locale.setDefault(Locale.ENGLISH);
                break;
            }
            ConsoleHelper.writeMessage(res.getString("wrong.input"));
        }

        ConsoleHelper.reloadResources();
        CommandExecutor.reloadAllResources();
    }

    @Override
    public void reloadResources() {
        res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "language");
    }
}
