package command;

import exception.InterruptOperationException;
import exception.ReturnToMainException;

/**
 * @author Alterovych Ilya
 */
interface Command {
    public void execute() throws InterruptOperationException, ReturnToMainException;
    public void reloadResources();
}
