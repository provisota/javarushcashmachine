package command;

import exception.InterruptOperationException;
import exception.NotEnoughMoneyException;
import exception.ReturnToMainException;
import main.CashMachine;
import main.ConsoleHelper;
import main.CurrencyManipulator;
import main.CurrencyManipulatorFactory;

import java.util.Comparator;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;

/**
 * @author Alterovych Ilya
 */
class WithdrawCommand implements Command {
    private ResourceBundle res =
            ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "withdraw");

    @Override
    public void execute() throws InterruptOperationException, ReturnToMainException {

        ConsoleHelper.writeMessage(res.getString("before"));
        String currencyCode;
        while (true) {
            currencyCode = ConsoleHelper.askCurrencyCode();
            if (CurrencyManipulatorFactory.isCurrencyPresent(currencyCode))
                break;
            ConsoleHelper.writeMessage(res.getString("currency.not.available"));
        }
        CurrencyManipulator currencyManipulator = CurrencyManipulatorFactory
                .getManipulatorByCurrencyCode(currencyCode);

        Integer sum;
        Map<Integer, Integer> withdrawDenominations;
        boolean success = false;

        do{
            ConsoleHelper.writeMessage(res.getString("specify.amount"));
            try {
                sum = Integer.valueOf(ConsoleHelper.readString());
            } catch (NumberFormatException e) {
                ConsoleHelper.writeMessage(res.getString("specify.not.empty.amount"));
                continue;
            }
            if (!currencyManipulator.isAmountAvailable(sum)){
                ConsoleHelper.writeMessage(res.getString("not.enough.money"));
                continue;
            }
            try {
                withdrawDenominations =
                        currencyManipulator.withdrawAmount(sum);
            } catch (NotEnoughMoneyException e) {
                ConsoleHelper.writeMessage(res.getString("exact.amount.not.available"));
                continue;
            }
            printResult(withdrawDenominations);
            ConsoleHelper.writeMessage(String.format
                    (res.getString("success.format"), sum, currencyCode));
            success = true;

        } while (!success);
    }

    @Override
    public void reloadResources() {
        res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "withdraw");
    }

    private void printResult(Map<Integer, Integer> result){
        Map <Integer, Integer> treeMap = new TreeMap<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2.compareTo(o1);
            }
        });
        treeMap.putAll(result);

        for (Integer denomination : treeMap.keySet()){
            ConsoleHelper.writeMessage("\t" + denomination
                    + " - " + treeMap.get(denomination));
        }
    }
}
