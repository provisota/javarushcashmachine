package command;

import main.CashMachine;
import main.ConsoleHelper;
import main.CurrencyManipulator;
import main.CurrencyManipulatorFactory;

import java.util.ResourceBundle;

/**
 * @author Alterovych Ilya
 */
class InfoCommand implements Command {
    private ResourceBundle res =
            ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "info");
    @Override
        public void execute() {
        ConsoleHelper.writeMessage(res.getString("before"));

        for (CurrencyManipulator currencyManipulator :
                CurrencyManipulatorFactory.getAllCurrencyManipulators()) {
            if (currencyManipulator.hasMoney()) {
                ConsoleHelper.writeMessage(currencyManipulator.getCurrencyCode()
                                        + " - " + currencyManipulator.getTotalAmount());
            } else {
                ConsoleHelper.writeMessage(res.getString("no.money"));
            }
        }
    }

    @Override
    public void reloadResources() {
        res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "info");
    }
}
