package command;

import exception.InterruptOperationException;
import exception.ReturnToMainException;
import main.CashMachine;
import main.ConsoleHelper;

import java.util.Enumeration;
import java.util.ResourceBundle;

/**
 * @author Alterovych Ilya
 */
public class LoginCommand implements Command {
    long cardNumber;
    int cardPin;
    private ResourceBundle validCreditCards =
            ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "verifiedCards");
    private ResourceBundle res =
            ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "login");

    @Override
    public void execute() throws InterruptOperationException, ReturnToMainException {

        ConsoleHelper.writeMessage(res.getString("before"));
        ConsoleHelper.writeMessage(res.getString("specify.data"));
        boolean valid = false;
        while (!valid) {
            String line1;
            String line2;
            long number = 0;
            int pin = 0;

            boolean correct = false;
            while (!correct) {
                line1 = ConsoleHelper.readString();
                line2 = ConsoleHelper.readString();

                if (line1.length() != 12 || line2.length() != 4) {
                    ConsoleHelper.writeMessage(res.getString("try.again.or.exit"));
                    continue;
                }

                try {
                    number = Long.parseLong(line1);
                    pin = Integer.parseInt(line2);
                } catch (NumberFormatException e) {
                    ConsoleHelper.writeMessage(res.getString("try.again.or.exit"));
                }
                correct = true;
            }

            Enumeration<String> keys = validCreditCards.getKeys();
            while (keys.hasMoreElements()) {
                String key = keys.nextElement();
                cardNumber = Long.parseLong(key);
                cardPin = Integer.parseInt(validCreditCards.getString(key));
                if (cardNumber == number && cardPin == pin) {
                    ConsoleHelper.writeMessage(String.format
                            (res.getString("success.format"), cardNumber));
                    valid = true;
                    break;
                }
            }

            if (!valid) {
                ConsoleHelper.writeMessage(String.format
                        (res.getString("not.verified.format"), number));
                ConsoleHelper.writeMessage(res.getString("try.again.with.details"));
            }
        }
    }

    @Override
    public void reloadResources() {
        res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "login");
    }
}
