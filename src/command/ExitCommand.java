package command;

import exception.InterruptOperationException;
import exception.ReturnToMainException;
import main.CashMachine;
import main.ConsoleHelper;

import java.util.ResourceBundle;

/**
 * @author Alterovych Ilya
 */
class ExitCommand implements Command {
    private ResourceBundle res =
            ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "exit");
    @Override
    public void execute() throws InterruptOperationException, ReturnToMainException {

        ConsoleHelper.writeMessage(res.getString("exit.question.y.n"));
        String confirmation = ConsoleHelper.readString();
        if (res.getString("yes").equals(confirmation)) {
            ConsoleHelper.writeMessage(res.getString("thank.message"));
            System.exit(0);
        }
    }

    @Override
    public void reloadResources() {
        res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "exit");
    }
}
