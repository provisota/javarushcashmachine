import java.io.*;
import java.util.ArrayList;

/**
 * @author Alterovych Ilya
 */
public class FileEncoding {
    // fileName - absolute path any file or directory contains files to encode
    private static String fileName =
        "D:\\IdeaProjects\\JavaRushCashMachine\\src\\resources";
    private static final char[] hexChar = { '0', '1', '2', '3', '4', '5', '6',
            '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

    public static void main(String[] args) {

        File path = new File(fileName);
        File[] files = path.listFiles();
        if (files == null) {
            files = new File[1];
            files[0] = path;
        }

        for (File file : files) {
            fileName = file.getAbsolutePath();
            ArrayList<String> stringList = new ArrayList<>();
            try {
                BufferedReader reader = new BufferedReader
                        (new InputStreamReader(new FileInputStream(fileName), "Cp1251"));
                while (reader.ready()) {
                    String newLine = convertToAscii(reader.readLine());
                    if (reader.ready()) {
                        newLine += "\r\n";
                    }
                    stringList.add(newLine);
                }
                reader.close();

                BufferedWriter writer = new BufferedWriter
                        (new OutputStreamWriter(new FileOutputStream(fileName), "Cp1251"));
                for (String line : stringList) {
                    writer.write(line);
                    writer.flush();
                }
                writer.close();
                print(fileName + " done...");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        print("finished successfully!");
    }

    static void print (String string){
        System.out.println(string);
    }

    static String convertToAscii(String string){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            char c = string.charAt(i);
            if ((c >> 7) > 0) {
                sb.append("\\u");
                // append the hex character for the left-most 4-bits
                sb.append(Character.toLowerCase(hexChar[(c >> 12) & 0xF]));
                // hex for the second group of 4-bits from the left
                sb.append(Character.toLowerCase(hexChar[(c >> 8) & 0xF]));
                // hex for the third group
                sb.append(Character.toLowerCase(hexChar[(c >> 4) & 0xF]));
                // hex for the last group, e.g., the right most 4-bits
                sb.append(Character.toLowerCase(hexChar[c & 0xF]));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }
}
